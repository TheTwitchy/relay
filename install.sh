#!/bin/sh
sudo pip3 install -r requirements.txt
sudo cp relay.py /usr/local/bin/
sudo cp relay.service /lib/systemd/system/
sudo systemctl enable relay
