#!/usr/bin/env python3

#Application global vars
#TODO Set global vars.
VERSION = "0.1"
PROG_NAME = "relay"
PROG_DESC = "A script to turn a Raspberry Pi into a simple communications client."
PROG_EPILOG = "Written by TheTwitchy. Source available at gitlab.com/TheTwitchy/relay"
DEBUG = True

#Application imports.
import sys,signal
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import time
import re

try:
    import argparse
except ImportError:
    print_err("Failed to import argparse module. Needs python 2.7+.")
    quit()
#Try to import termcolor, ignore if not available.
DO_COLOR = True
try:
    import termcolor
except ImportError:
    DO_COLOR = False
def try_color(string, color):
    if DO_COLOR:
        return termcolor.colored(string, color)
    else:
        return string
#Print some info to stdout
def print_info(outfile, *args):
    sys.stdout.write(try_color("info: ", "green"))
    sys.stdout.write(try_color(" ".join(map(str,args)) + "\n", "green"))

    if not outfile == "":
        fd = open(outfile, "a")
        fd.write("info: " + " ".join(map(str,args)) + "\n")
#Print an error to stderr
def print_err(outfile, *args):
    sys.stderr.write(try_color("error: ", "red"))
    sys.stderr.write(try_color(" ".join(map(str,args)) + "\n", "red"))

    if not outfile == "":
        fd = open(outfile, "a")
        fd.write("error: " + " ".join(map(str,args)) + "\n")

#Print a debug statement to stdout
def print_debug(outfile, *args):
    if DEBUG:
        sys.stderr.write(try_color("debug: ", "blue"))
        sys.stderr.write(try_color(" ".join(map(str,args)) + "\n", "blue"))

    if not outfile == "":
        fd = open(outfile, "a")
        fd.write("debug: " + " ".join(map(str,args)) + "\n")
#Handles early quitters.
def signal_handler(signal, frame):
    print("")
    quit(0)

#Because.
def print_header():
    print("                                             ")
    print("                     _|                      ")
    print(" _|  _|_|    _|_|    _|    _|_|_|  _|    _|  ")
    print(" _|_|      _|_|_|_|  _|  _|    _|  _|    _|  ")
    print(" _|        _|        _|  _|    _|  _|    _|  ")
    print(" _|          _|_|_|  _|    _|_|_|    _|_|_|  ")
    print("                                         _|  ")
    print("                                     _|_|    ")
    print("")
    print("                                       v" + VERSION)
    print("")

# Argument parsing which outputs a dictionary.
def parseArgs():
    #Setup the argparser and all args
    parser = argparse.ArgumentParser(prog=PROG_NAME, description=PROG_DESC, epilog=PROG_EPILOG)
    parser.add_argument("-v", "--version", action="version", version="%(prog)s " + VERSION)
    parser.add_argument("-q", "--quiet", help="surpress all non-error print to screen", action="store_true", default=False)
    parser.add_argument("-i", "--interval", help="test interval, in seconds", default = 5)
    parser.add_argument("--insecure", help="all HTTPS requests will not validate connection security", action="store_true", default=False)
    parser.add_argument("--http", help="make an HTTP(S) GET request to an endpoint")
    parser.add_argument("-o", "--out", help="output file", default = "")
    return parser.parse_args()


def http_req(endpoint, verify_cert=True):
    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    response = session.get(endpoint, verify=verify_cert)
    return response.status_code

#Main application entry point.
def main():
    #Signal handler to catch CTRL-C (quit immediately)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    argv = parseArgs()

    #Print out some sweet ASCII art.
    if not argv.quiet:
        print_header()

    if argv.http:
        if not re.search(r"^http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+$", argv.http):
            print_err(argv.out, "The HTTP(S) endpoint is in the incorrect format")
            quit(-1)

    # Use this check to make sure we're performing at least one test
    if not argv.http:
        print_err(argv.out, "No test type has been specified")
        quit(-1)

    if not argv.quiet:
        print_info(argv.out, "Beginning test procedures. Use CTRL-C to exit.")
    
    while True:
        if argv.http:
            ret = http_req(argv.http, verify_cert=not argv.insecure)
            if ret == 0:
                print_err(argv.out, "Failure in HTTP test to %s" % argv.http)
            elif ret == 200 and not argv.quiet:
                print_info(argv.out, "Success in HTTP test to %s" % argv.http)
            else:
                if not argv.quiet:
                    print_info(argv.out, "General failure in HTTP test to %s, returned status code %d" % (argv.http, ret))

        time.sleep(argv.interval)

if __name__ == "__main__":
    main()
    quit(0)
